#!/usr/bin/env python3

import rospy
from std_msgs.msg import Int32

node2_number = None
pub = rospy.Publisher('result_312553', Int32, queue_size=10)

def callback(data):
    global node2_number
    if node2_number is None:
        node2_number = data.data
    else:
        result = node2_number + data.data
        rospy.loginfo(f"Two numbers were received: node 2 - {node2_number}; node3 - {data.data} -> result: {node2_number} + {data.data} = {result}")
        pub.publish(result)
        node2_number = None

def node1():
    rospy.init_node('node1', anonymous=True)
    rospy.Subscriber('data_transfer_topic', Int32, callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        node1()
    except rospy.ROSInterruptException:
        pass
    