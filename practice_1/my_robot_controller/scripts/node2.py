#!/usr/bin/env python3

import rospy
from std_msgs.msg import Int32

def node2():
    rospy.init_node('node2', anonymous=True)
    node2 = rospy.Publisher('data_transfer_topic', Int32, queue_size=10)
    rate = rospy.Rate(10)  # 10 Hz

    while not rospy.is_shutdown():
        number = int(input("Enter a number for node2: "))
        rospy.loginfo(f"Entered number for node2 - {number}")
        node2.publish(number)
        rate.sleep()

if __name__ == '__main__':
    try:
        node2()
    except rospy.ROSInterruptException:
        pass
